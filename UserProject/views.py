from .models import Project
from django.contrib.auth.models import Group
from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from django.forms import DateInput
from django.urls import reverse
from guardian.shortcuts import assign_perm
from guardian.mixins import PermissionRequiredMixin, PermissionListMixin
from .utils import unique_slug_generator

# Create your views here.



class ProjectList( PermissionListMixin, ListView):
    permission_required = 'UserProject.view_project'

    model = Project

class ProjectView( PermissionRequiredMixin, DetailView):
    permission_required = 'UserProject.view_project'
    model = Project

    def get_context_data(self, **kwargs):
        test = self.request.user.has_perm('UserProject.view_project', self.object)
        if (self.request.user.has_perm('UserProject.view_project', self.object)):
            print('has access')
        else:
            print('no access')
        return super().get_context_data(**kwargs)



class ProjectCreate(CreateView):
    model = Project
    fields = ['name', 'short_description', 'description', 'logo', 'end_date', 'skills','visibility','discussion_forum',
              'public_discussion','public_can_vote','observer_log_comment','observer_add_wishlist','slug']
    widgets = {
        'end_date': DateInput(attrs={'type': 'date'})
    }

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['end_date'].widget = DateInput(attrs={'type': 'date'})
        return form

    def form_valid(self, form):
        form.instance.author = self.request.user
        # If the slug is empty autogenerate a slug
        if form.cleaned_data['slug'].isspace() or not form.cleaned_data['slug']:
            form.instance.slug = unique_slug_generator(form.instance)

        response =   super().form_valid(form)

        ownerGroup,_ = Group.objects.get_or_create(name='ProjectOwner')
        collaboratorGroup, _ = Group.objects.get_or_create(name='ProjectCollaborator')
        observerGroup, _ = Group.objects.get_or_create(name= 'ProjectObserver_' + self.object.slug)

        assign_perm('UserProject.view_project', ownerGroup, self.object)
        assign_perm('UserProject.change_project', ownerGroup, self.object)
        assign_perm('UserProject.delete_project', ownerGroup, self.object)
        assign_perm('UserProject.can_add_collaborator', ownerGroup, self.object)

        assign_perm('UserProject.view_project', collaboratorGroup, self.object)
        assign_perm('UserProject.can_add_observer', ownerGroup, self.object)

        assign_perm('UserProject.view_project', observerGroup, self.object)

        self.request.user.groups.add(ownerGroup)

        return response




class ProjectUpdate(UpdateView):
    model = Project
    fields = ['name', 'short_description', 'description', 'logo', 'end_date', 'skills','visibility','discussion_forum',
              'public_discussion','public_can_vote','observer_log_comment','observer_add_wishlist','slug']

    def get_success_url(self):
            return reverse('UserProject:project_view', kwargs = {'slug':self.object.slug })


    def form_valid(self, form):
        form.instance.author = self.request.user
        # If the slug is empty autogenerate a slug
        if form.cleaned_data['slug'].isspace() or not form.cleaned_data['slug']:
            form.instance.slug = unique_slug_generator(form.instance)

        return  super().form_valid(form)



class ProjectDelete(PermissionRequiredMixin, DeleteView):
    permission_required = 'UserProject.delete_project'
    model = Project

    def get_success_url(self):
        return reverse('UserProject:project_list')
