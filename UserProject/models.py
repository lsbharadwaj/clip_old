from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

# Create your models here.
class Project(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='author',
                               help_text='Project creator name')
    collaborators = models.ManyToManyField(User, blank=True , related_name='collaborator')
    observers = models.ManyToManyField(User, blank=True ,  related_name='observers')
    name = models.CharField(max_length=20, help_text='Name of the project')
    short_description = models.CharField(max_length=100, help_text='A one line description for the project', blank=True)
    description = models.TextField(blank=True, help_text='Introduce your project here...')
    logo = models.ImageField(blank=True, help_text='Provide a logo',  upload_to='projectProfile')
    created_date = models.DateField(blank=True, auto_now_add=True)
    end_date = models.DateField()
    skills = models.CharField(max_length=100, blank=True)
    visibility = models.CharField(max_length=1, choices=(
        ('1', 'Public'),
        ('2', 'Restricted'),
        ('3', 'Private')
    ), default=('1', 'Public'), help_text='Control who can see the project. Public => Anyone, '
                                          'Private => Only those invited, '
                                          'Restricted => Like private but people can apply for being an observer or a team member')
    discussion_forum = models.BooleanField(default=False, help_text="Allow discussion forum for general discussions over project")
    public_discussion = models.BooleanField(default=False, help_text='Allow non observers to participate in discussions in the form')
    public_can_vote = models.BooleanField(default=False, help_text='Allow public to vote in the wishlist')
    observer_log_comment = models.BooleanField(default=False, help_text='Allow observers to comment in the logs')
    observer_add_wishlist = models.BooleanField(default=False, help_text='Allow observers to add wish')
    slug = models.SlugField(max_length=50, db_index=True, allow_unicode=False, blank=True, verbose_name='Project URL', unique=True, help_text='Leave blank to autogenerate')

    class Meta:
        permissions = (
        ('can_add_collaborator', 'Can add collaborator'),
        ('can_add_observer', 'Can add observer'),
        )


    def __str__(self):
        return self.name


    def get_absolute_url(self):
        return reverse('UserProject:project_view', args=[str(self.slug)])
