# Generated by Django 2.1 on 2018-09-30 08:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('UserProject', '0006_auto_20180929_1439'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='short_description',
            field=models.CharField(blank=True, help_text='A one line description for the project', max_length=100),
        ),
        migrations.AlterField(
            model_name='project',
            name='name',
            field=models.CharField(help_text='Name of the project', max_length=20),
        ),
    ]
