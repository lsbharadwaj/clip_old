from django.forms import ModelForm, DateInput
from .models import Project

class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ['name', 'description', 'logo', 'lic', 'end_date', 'skills']
        widgets = {'end_date':DateInput(attrs={'type':'date'})}