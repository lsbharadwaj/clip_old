from django.urls import path
from . import views

app_name = 'UserProject'

urlpatterns = [
    path('', views.ProjectList.as_view(), name='project_list'),
    path('view/<slug:slug>', views.ProjectView.as_view(), name='project_view'),
    path('new', views.ProjectCreate.as_view(), name='project_create'),
    path('edit/<slug:slug>', views.ProjectUpdate.as_view(), name='project_edit'),
    path('delete/<slug:slug>', views.ProjectDelete.as_view(), name='project_delete'),

]
