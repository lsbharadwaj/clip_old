from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from allauth.socialaccount.models import SocialAccount

# Create your views here.
class showProfile(LoginRequiredMixin, TemplateView):
    template_name = 'UserProfile/profile.html'
    def get_context_data(self):
        context={'user':self.request.user}
        return context