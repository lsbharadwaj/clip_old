from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

app_name = 'UserProfile'

urlpatterns = [
    path('', views.showProfile.as_view(), name='profile'),
]
