certifi==2018.8.13
chardet==3.0.4
defusedxml==0.5.0
dj-database-url==0.5.0
Django==2.1
django-adminlte2==0.4.1
django-allauth==0.37.1
django-crispy-forms==1.7.2
django-debug-toolbar==1.10
django-guardian==1.4.9
django-heroku==0.3.1
django-js-asset==1.1.0
django-mptt==0.9.1
django-widget-tweaks==1.4.2
gunicorn==19.9.0
idna==2.7
oauthlib==2.1.0
Pillow==5.2.0
psycopg2==2.7.5
python3-openid==3.1.0
pytz==2018.5
requests==2.19.1
requests-oauthlib==1.0.0
six==1.11.0
sqlparse==0.2.4
urllib3==1.23
whitenoise==4.0
